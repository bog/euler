#include <iostream>
#include <cassert>

/**
 * Test if a given positive number can be divised by all the numbers from 1 to a
 * positive none null upper bound.
 * @param m the given positive number.
 * @param n the upper bound greater than zero.
 * @return true if m is divisible by all the numbers in [1; n].
 */
bool is_divisible_from_1_to_n(int m, int n);

/**
 * Get the factorial of a given number.
 * @param n the given number.
 * @return n!.
 */
int factorial(int n);

/**
 * Get the smallest number divisible by all the terms between one and a
 * given upper bound.
 * @param n a given positive not null upper bound.
 * @return the smallest number such that it can be evenly divided by
 *         all the numbers in [1 ; n].
 */
int smallest_divisible_from_1_to_n(int n);
/**
 * Project Euler
 * Problem number 5 :
 * 
 * 2520 is the smallest number that can be divided by 
 * each of the numbers from 1 to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by
 *  all of the numbers from 1 to 20?
 *
 * @author bog
 * @date 23/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 20;
  int res = smallest_divisible_from_1_to_n(N);
  std::cout<< "result : " << res <<std::endl;
  return 0;
}

bool is_divisible_from_1_to_n(int m, int n)
{
  assert(m >= 0);
  assert(n > 0);
  
  bool divisible = true;
  int i = 2;

  while (i < n && divisible)
    {
      divisible = (m%i == 0);
      i++;
    }

  return divisible;
}

int factorial(int n)
{
  assert(n >= 0);
  
  if (n == 0) { return 1; }
  else { return n * factorial(n-1); }
}

int smallest_divisible_from_1_to_n(int n)
{
  assert(n > 0);
  int i = 1;
  int smallest = -1;
  bool found = false;
  
  while (!found)
    {
      if ( is_divisible_from_1_to_n(i, n) )
	{
	  smallest = i;
	  found = true;
	}
      
      i++;
    }

  assert(found);

  return smallest;
}
