#include <iostream>

/**
 * Project Euler
 * Problem number 2 :
 * By considering the terms in the Fibonacci sequence 
 * whose values do not exceed four million, find the sum
 * of the even-valued terms.
 *
 * @author bog
 * @date 23/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int f0 = 1;
  int f1 = 1;
  int const LIMIT = 4000000;
  int sum = 0;

  while (f1 < LIMIT)
    {
      // Compute the next term of fibonacci sequence.
      int tmp = f1;      
      f1 += f0;
      f0 = tmp;

      if (f1%2 == 0)
	// Considering even terms :
	{
	  // compute their sum.
	  sum += f1;
	}      
    }

  std::cout<< "result : " << sum <<std::endl;
  return 0;
}
