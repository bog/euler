.PHONY: doc clean

all:doc

doc:
	doxygen
clean:
	rm -rf doc/*
