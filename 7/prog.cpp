#include <iostream>
#include <cmath>
#include <cassert>

/**
 * Test if a number is a prime one.
 * @deprecated naive implementation.
 * @param n the number to test.
 * @return true if n is a prime number, false otherwise.
 */
bool is_prime(int n);

/**
 * Get the nth prime number.
 * @param n the rank of the prime number.
 * @return the nth prime number.
 */
long int prime_nth(int n);

/**
 * Project Euler
 * Problem number 7 :
 * 
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
 * we can see that the 6th prime is 13.
 *
 * What is the 10 001st prime number?
 *
 * @author bog
 * @date 25/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  long int const N = 10001;
  std::cout<< "result : " << prime_nth(N) <<std::endl;
  return 0;
}

bool is_prime(int n)
{
  assert(n > 0);
  
  if (n < 2) { return false; }

  int square = sqrt(n);
    
  for (int i=2; i<=square; i++)
    {
      if (n%i == 0)
	{
	  return false;
	}
    }
  
  return true;
}

long int prime_nth(int n)
{
  int i = 0;
  int current = 2;
  
  while(i < n)
    {
      if ( is_prime(current) )
	{
	  i++;
	}
      
      current++;
    }

  return current - 1;
}
