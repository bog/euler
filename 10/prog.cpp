#include <iostream>
#include <cmath>
#include <vector>

/**
 * List of the primes under a given maximum.
 * @param n the maximum number to test.
 * @return the list of the primes under n.
 */
std::vector<long> primes_under_n(int n);

/**
 * Return the sum of all primes.
 * @param primes the list of prime numbers.
 * @return the sum of the prime numbers.
 */
long primes_sum(std::vector<long> const& primes);

/**
 * Project Euler
 * Problem number 10 :
 * 
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 *
 * @author bog
 * @date 9/08/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 2000000;
  std::cout<< primes_sum( primes_under_n(N) ) <<std::endl;
  
  return 0;
}


std::vector<long> primes_under_n(int n)
{
  std::vector<long> primes;
  primes.push_back(2);

  for (int i=3; i<n; i++)
    {
      bool is_prime = true;
      size_t j = 0;
      long square_root = sqrt(i);
      
      while (j<primes.size() && primes[j] <= square_root && is_prime)
	{
	  if (i%primes[j] == 0)
	    {
	      is_prime = false;	      
	    }
	  
	  j++;
	}

      if (is_prime)
	{
	  primes.push_back(i);
	}
    }
  
  return primes;
}

long primes_sum(std::vector<long> const& primes)
{
  long sum = 0;
  
  for (long p : primes)
    {
      sum += p;
    }

  return sum;
}
