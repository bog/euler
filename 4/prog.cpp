#include <iostream>
#include <cassert>
#include <cmath>
#include <vector>

/**
 * Decompose a number into a base ten digits array.
 * @param n the number to decompose.
 * @return the array containing digits such that 
 *         the sum of the arr[i] * 10^i equals n.
 */
std::vector<int> decompose(int n);

/**
 * Test whether a given positive number is a palindrom.
 * @param n the number to test.
 * @return true if n is a palindrom, false otherwise.
 */
bool is_palindrom(int n);

/**
 * Get the largest palindrom made from a product of two numbers of
 * n digits.
 * @param n the palindrom product number digits.
 * @return the largest palindrom made from two n-digits numbers.
 */
int largest_n_digits_product_palindrom(int n);

/**
 * Project Euler
 * Problem number 4 :
 * a palindromic number reads the same both ways.
 * The largest palindrome made from the product of 
 * two 2-digit numbers is 9009 = 91 × 99.
 *
 * find the largest palindrome made from the product of 
 * two 3-digit numbers.
 *
 * @author bog
 * @date 23/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 3;

  int res = largest_n_digits_product_palindrom(N);
  std::cout<<"result : " << res <<std::endl;
  return 0;
}

std::vector<int> decompose(int n)
{
  assert(n >= 0);
  
  std::vector<int> arr;
  int number = n;
  
  while (number > 0)
    {
      int digit = number - (number/10) * 10;
      
      number -= digit;
      number /= 10;
      
      arr.push_back(digit);
    }
  

  return arr;
}

bool is_palindrom(int n)
{
  assert(n >= 0);
  
  if (n == 0) { return true; }
  
  std::vector<int> digits = decompose(n);
  int i = 0;
  bool palindrom = true;
  
  while (i < static_cast<int>(digits.size()/2) + 1 &&
	 palindrom)
    {
      int first = digits[i];
      int last = digits[digits.size() - i - 1];

      if (first != last)
      	{
      	  palindrom = false;
      	}
      
      i++;
    }

  return palindrom;
}

int largest_n_digits_product_palindrom(int n)
{
  int max = 0;
  int product_digits = pow(10, n);
  
  for (int i=0; i<product_digits; i++)
    {
      for (int j=0; j<product_digits; j++)
	{
	  int n = i*j;
	  if (n > max && is_palindrom(n))
	    {
	      max = n;
	    }
	}
    }

  return max;
}
