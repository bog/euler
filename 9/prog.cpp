#include <iostream>
#include <cmath>

/**
 * Project Euler
 * Problem number 9 :
 * 
 * 
 *  A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 *  a2 + b2 = c2
 * 
 *  For example, 32 + 42 = 9 + 16 = 25 = 52.
 * 
 *  There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 *  Find the product abc.
 * 
 *
 * @author bog
 * @date 9/08/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 1000;
  
  for (int a=0; a<N; a++)
    {
      for (int b=a + 1; b<N; b++)
	{
	  for (int c=b + 1; c<N; c++)
	    {
	      if (a*a + b*b == c*c &&
		  a + b + c == 1000)
		{
		  std::cout<< (a*b*c) <<std::endl;

		  return 0;
		}
	    }
	}
    }
  
  return 0;
}
