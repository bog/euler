#include <iostream>

/**
 * Project Euler
 * Problem number 1 :
 * find the sum of all the multiples of 3 or 5 below 1000.
 *
 * @author bog
 * @date 23/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 1000;
  int sum = 0;
  
  for (int i=0; i<N; i++)
    {
      if (i % 3 == 0 || i % 5 == 0)
	{
	  sum += i;
	}
    }

  std::cout<< "result : " << sum <<std::endl;
  
  return 0;
}
