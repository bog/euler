#!/bin/sh
# Création d'un nouveau projet.
# Paramètre : n, le numéro du projet.

if [ $# -lt 1 ]
then
    echo "E : The problem number must be specified."
    exit -1
fi

N="$1"

if [ -d "$N" ]
then
    echo "E : The problem number $N already exists."
    exit -1
fi

cp -R pb "$N"
