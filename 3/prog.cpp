#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

/**
 * Test if a given number is whether prime number or not.
 * @deprecated naive implementation.
 * @param n the number to test.
 * @return true if n is a prime number, false otherwise.
 */
bool is_prime(long int n);

/**
 * List the first prime numbers lower than n.
 * @param n the upper bound.
 * @return a vector containing those prime numbers.
 */
std::vector<int> first_primes(long int n);

/**
 * Get the greatest prime factor of a given number.
 * @param n the number to test.
 * @return the largest prime factor.
 */
int largest_prime_factor(long int n);

/**
 * Project Euler
 * Problem number 3 :
 * largest prime factor of the number 600851475143.
 *
 * @author bog
 * @date 23/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  long int const N = 600851475143;

  long int largest_prime = largest_prime_factor(N);
  
  std::cout<< "result : " << largest_prime <<std::endl;
 
  
  return 0;
}

bool is_prime(long int n)
{
  int square_root = static_cast<int>( sqrt(n) );
  
  for (int i=2; i<=square_root; i++)
    {
      if (n%i == 0)
	{
	  return false;
	}
    }

  return true;
}

std::vector<int> first_primes(long int n)
{
  assert(n > 0);
  
  std::vector<int> primes;
  
  int current = 2;
  
  while (primes.empty() || primes.back() < n)
    {
      if (is_prime(current))
	{
	  primes.push_back(current);
	}
      
      current++;
    }

  return primes;
}

int largest_prime_factor(long int n)
{
  std::vector<int> primes =
    first_primes( static_cast<int>( sqrt(n) ));
  long int largest_prime = -1;
  long int number = n;
  
  for (int prime : primes)
    {
      if (number%prime == 0)
	{
	  while (number%prime == 0)
	    {
	      number /= prime;
	    }
	  
	  largest_prime = prime;
	}
    }

  assert(largest_prime != -1);
  
  return largest_prime;
}
