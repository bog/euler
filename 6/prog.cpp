#include <iostream>

/**
 * The sum of the squares of a given number of natural numbers.
 * @param n the upper bound limit.
 * @return the sum of the n first square.
 */
int sum_of_square(int n);

/**
 * The square of the sum of a given number of natural numbers.
 * @param n the upper bound limit.
 * @return the square of the sum of the first n numbers.
 */
int square_of_sum(int n);

/**
 * Project Euler
 * Problem number 6 :
 * 
 * The sum of the squares of the first ten natural numbers is,
 * 1^2 + 2^2 + ... + 10^2 = 385
 *
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)^2 = 552 = 3025
 *
 * Hence the difference between the sum of the squares of the first
 * ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 *
 * Find the difference between the sum of the squares of 
 * the first one hundred natural numbers and the square of the sum.
 *
 * @author bog
 * @date 25/06/18
 * @version 1
 */
int main(int argc, char** argv)
{
  int const N = 100;
  int result = square_of_sum(N) - sum_of_square(N);
  std::cout<< "result : " << result <<std::endl;
  return 0;
}

int sum_of_square(int n)
{
  int sum = 0;
  
  for (int i=1; i<=n; i++)
    {
      sum += i * i;
    }

  return sum;
}

int square_of_sum(int n)
{
  int sum = 0;
  
  for (int i=0; i<= n; i++)
    {
      sum += i;
    }

  return sum * sum;
}
